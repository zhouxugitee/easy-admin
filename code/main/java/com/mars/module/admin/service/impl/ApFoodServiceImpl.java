package com.mars.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.request.ApFoodRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.admin.mapper.ApFoodMapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.admin.entity.ApFood;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.admin.service.IApFoodService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 美食业务层处理
 *
 * @author mars
 * @date 2024-03-15
 */
@Slf4j
@Service
@AllArgsConstructor
public class ApFoodServiceImpl implements IApFoodService {

    private final ApFoodMapper baseMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApFood add(ApFoodRequest request) {
        ApFood entity = ApFood.builder().build();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Long id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(List<Long> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(ApFoodRequest request) {
        ApFood entity = ApFood.builder().build();
        BeanUtils.copyProperties(request, entity);
        return baseMapper.updateById(entity) > 0 ;
    }

    @Override
    public ApFood getById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public PageInfo<ApFood> pageList(ApFoodRequest request) {
        Page<ApFood> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<ApFood> query = this.buildWrapper(request);
        IPage<ApFood> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }


    @Override
    public List<ApFood> list(ApFoodRequest request) {
        LambdaQueryWrapper<ApFood> query = this.buildWrapper(request);
        return baseMapper.selectList(query);
    }

    private LambdaQueryWrapper<ApFood> buildWrapper(ApFoodRequest param) {
        LambdaQueryWrapper<ApFood> query = new LambdaQueryWrapper<>();
         if (StringUtils.isNotBlank(param.getName())){
               query.like(ApFood::getName ,param.getName());
        }
         if (param.getPrice() != null){
               query.like(ApFood::getPrice ,param.getPrice());
        }
         if (StringUtils.isNotBlank(param.getPicture())){
               query.like(ApFood::getPicture ,param.getPicture());
        }
         if (StringUtils.isNotBlank(param.getIntroduce())){
               query.like(ApFood::getIntroduce ,param.getIntroduce());
        }
        return query;
    }

}
