package com.mars.module.admin.service;

import com.mars.module.admin.entity.ApFood;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.ApFoodRequest;

import java.util.List;

/**
 * 美食接口
 *
 * @author mars
 * @date 2024-03-15
 */
public interface IApFoodService {
    /**
     * 新增
     *
     * @param param param
     * @return ApFood
     */
    ApFood add(ApFoodRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(ApFoodRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return ApFood
     */
    ApFood getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<ApFood>
     */
    PageInfo<ApFood> pageList(ApFoodRequest param);


    /**
     * 查询所有数据
     *
     * @return List<ApFood>
     */
    List<ApFood> list(ApFoodRequest param);
}
