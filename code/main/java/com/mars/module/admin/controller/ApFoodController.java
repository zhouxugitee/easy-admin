package com.mars.module.admin.controller;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import com.mars.common.util.ExcelUtils;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.RateLimiter;
import com.mars.module.admin.entity.ApFood;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import com.mars.common.response.PageInfo;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.IApFoodService;
import com.mars.module.admin.request.ApFoodRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * 美食控制层
 *
 * @author mars
 * @date 2024-03-15
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "美食接口管理",tags = "美食接口管理")
@RequestMapping("/admin/apFood" )
public class ApFoodController {

    private final IApFoodService iApFoodService;

    /**
     * 分页查询美食列表
     */
    @ApiOperation(value = "分页查询美食列表")
    @PostMapping("/pageList")
    public R<PageInfo<ApFood>> pageList(@RequestBody ApFoodRequest apFoodRequest) {
        return R.success(iApFoodService.pageList(apFoodRequest));
    }

    /**
     * 获取美食详细信息
     */
    @ApiOperation(value = "获取美食详细信息")
    @GetMapping(value = "/query/{id}")
    public R<ApFood> detail(@PathVariable("id") Long id) {
        return R.success(iApFoodService.getById(id));
    }

    /**
     * 新增美食
     */
    @Log(title = "新增美食", businessType = BusinessType.INSERT)
    @RateLimiter
    @ApiOperation(value = "新增美食")
    @PostMapping("/add")
    public R<Void> add(@RequestBody ApFoodRequest apFoodRequest) {
        iApFoodService.add(apFoodRequest);
        return R.success();
    }

    /**
     * 修改美食
     */
    @Log(title = "修改美食", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改美食")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody ApFoodRequest apFoodRequest) {
        iApFoodService.update(apFoodRequest);
        return R.success();
    }

    /**
     * 删除美食
     */
    @Log(title = "删除美食", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除美食")
    @PostMapping("/delete/{ids}")
    public R<Void> remove(@PathVariable Long[] ids) {
        iApFoodService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }

    /**
    * 导出美食
    *
    * @param response response
    * @throws IOException IOException
    */
    @PostMapping(value = "/export")
    public void exportExcel(HttpServletResponse response,@RequestBody ApFoodRequest apFoodRequest) throws IOException {
        List<ApFood> list = iApFoodService.list(apFoodRequest);
        ExcelUtils.exportExcel(list, ApFood.class, "美食信息", response);
    }
}
