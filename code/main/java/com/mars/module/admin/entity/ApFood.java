package com.mars.module.admin.entity;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.*;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.mars.module.system.entity.BaseEntity;

/**
 * 美食对象 ap_food
 *
 * @author mars
 * @date 2024-03-15
 */

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "美食对象")
@Builder
@Accessors(chain = true)
@TableName("ap_food")
public class ApFood extends BaseEntity {

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;
    /**
     * 名称
     */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;
    /**
     * 价格
     */
    @Excel(name = "价格")
    @ApiModelProperty(value = "价格")
    private BigDecimal price;
    /**
     * 图片
     */
    @Excel(name = "图片")
    @ApiModelProperty(value = "图片")
    private String picture;
    /**
     * 介绍
     */
    @Excel(name = "介绍")
    @ApiModelProperty(value = "介绍")
    private String introduce;
}
