package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.ApFood;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 美食Mapper接口
 *
 * @author mars
 * @date 2024-03-15
 */
public interface ApFoodMapper extends BasePlusMapper<ApFood> {

}
