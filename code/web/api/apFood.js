/**
 * 分页查询美食列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apFood/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询美食详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apFood/query/' + id,
        method: 'get'
    })
}

/**
 * 新增美食
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apFood/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改美食
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apFood/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除美食
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apFood/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出美食
 *
 * @param query
 * @returns {*}
 */
function exportData(data) {
    return requests({
        url: '/admin/apFood/export',
        method: 'post',
        data: data
    })
}
