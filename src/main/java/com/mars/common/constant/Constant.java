package com.mars.common.constant;

/**
 * 常量
 *
 * @author 源码字节-程序员Mars
 */
public class Constant {

    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * 令牌
     */
    public static final String TOKEN = "Authorization";

    /**
     * cookie名称
     */
    public static final String COOKIE_NAME = "mars-cookie";

    /**
     * 验证码前缀
     */
    public static final String CAPTCHA_NAME = "captcha";

    /**
     * 设置默认密码
     */
    public static final String DEFAULT_PASSWORD = "123456";

    /**
     * 分隔符
     */
    public static final String COLON_SEPARATOR = ":";

    /**
     * 用户令牌
     */
    public static final String USER_TOKEN_CACHE = "user:token:";

    /**
     * html后缀
     */
    public static final String HTML = ".html";

    /**
     * 排序
     */
    public static final String SORT = "sort";

    /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT = "Logout";

    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL = "Error";
    /**
     * 接口地址
     */
    public static final String ESOBAO_URL = "http://ntoo8jie.esobao.cn/index.php/Ajax/ajaxList?leixingid=%s&quyuid=&num=%s&size=10";

    /**
     * 所有分类接口地址
     */
    public static final String ALL_ESOBAO_URL = "http://ntoo8jie.esobao.cn/index.php/Ajax/ajaxList?leixingid=&quyuid=&num=%s&size=10";

    /**
     * 默认头像
     */
    public static final String DEFAULT_AVATAR = "https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200";
}
