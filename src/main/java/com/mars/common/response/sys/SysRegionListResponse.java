package com.mars.common.response.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 省市区VO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class SysRegionListResponse {

    @ApiModelProperty(value = "ID")
    private Long id;

    @ApiModelProperty(value = "上一级ID")
    private Long pid;

    @ApiModelProperty(value = "名称")
    private String name;


}
