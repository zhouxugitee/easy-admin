package com.mars;

import com.mars.framework.annotation.EasyAdminApplications;
import org.springframework.boot.SpringApplication;

/**
 * Easy-Admin 启动类
 *
 * @author 源码字节-程序员Mars
 */
@EasyAdminApplications
public class EasyAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyAdminApplication.class, args);
    }
}
