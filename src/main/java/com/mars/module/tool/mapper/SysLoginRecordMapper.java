package com.mars.module.tool.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.module.tool.entity.SysLoginRecord;
import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.tool.request.SysLoginRecordRequest;
import org.apache.ibatis.annotations.Param;

/**
 * 系统访问记录Mapper接口
 *
 * @author mars
 * @date 2023-11-17
 */
public interface SysLoginRecordMapper extends BasePlusMapper<SysLoginRecord> {

    /**
     * 查询在线用户列表
     *
     * @param page    分页参数
     * @param request 请求参数
     * @return IPage<SysLoginRecord>
     */
    IPage<SysLoginRecord> selectOnLineUserPage(@Param("page") Page<SysLoginRecord> page, @Param("request") SysLoginRecordRequest request);
}
