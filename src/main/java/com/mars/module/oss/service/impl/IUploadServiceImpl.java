package com.mars.module.oss.service.impl;


import com.mars.framework.exception.ServiceException;
import com.mars.module.oss.common.enums.FileUploadTypeEnums;
import com.mars.module.oss.factory.StrategyFactory;
import com.mars.module.oss.service.IUploadService;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author 程序员Mars
 */
@Service
@Slf4j
public class IUploadServiceImpl implements IUploadService {

    @Value("${easy.admin.fileUploadType}")
    private String fileUploadType;


    @Override
    public String upload(MultipartFile file) throws Exception {
        if (StringUtils.isEmpty(fileUploadType)) {
            throw new ServiceException("文件上传类型为空");
        }
        FileUploadTypeEnums typeEnums = FileUploadTypeEnums.getFileEnumsByType(fileUploadType);
        String path = StrategyFactory.getStrategy(typeEnums).upload(file);
        log.info("上传类型：{},上传成功地址：{}", fileUploadType, path);
        return path;
    }
}
