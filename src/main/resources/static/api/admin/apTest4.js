/**
 * 分页查询测试4列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apTest4/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询测试4详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apTest4/query/' + id,
        method: 'get'
    })
}

/**
 * 新增测试4
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apTest4/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改测试4
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apTest4/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除测试4
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apTest4/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出测试4
 *
 * @param query
 * @returns {*}
 */
function exportTest4(query) {
    return requests({
        url: '/admin/apTest4/export',
        method: 'get',
        params: query
    })
}
