/**
 * 分页查询字典类型列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/sysDictType/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询字典类型详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/sysDictType/query/' + id,
        method: 'get'
    })
}

/**
 * 新增字典类型
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/sysDictType/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改字典类型
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/sysDictType/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除字典类型
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/sysDictType/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出字典类型
 *
 * @param query
 * @returns {*}
 */
function exportDictType(query) {
    return requests({
        url: '/admin/sysDictType/export',
        method: 'get',
        params: query
    })
}
