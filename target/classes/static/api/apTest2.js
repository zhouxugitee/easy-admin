/**
 * 分页查询测试2列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apTest2/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询测试2详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apTest2/query/' + id,
        method: 'get'
    })
}

/**
 * 新增测试2
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apTest2/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改测试2
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apTest2/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除测试2
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apTest2/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出测试2
 *
 * @param query
 * @returns {*}
 */
function exportTest2(query) {
    return requests({
        url: '/admin/apTest2/export',
        method: 'get',
        params: query
    })
}
