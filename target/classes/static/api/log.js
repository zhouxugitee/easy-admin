/**
 * 查询接口日志
 * @param query
 * @returns {*}
 */
function listLog(query) {
    return requests({
        url: '/log/api/list',
        method: 'post',
        data: query
    })
}

/**
 * 删除日志
 * @param id id
 * @returns {*}
 */
function deleteLogById(id) {
    return requests({
        url: '/log/api/delete/' + id,
        method: 'delete',
    })
}
