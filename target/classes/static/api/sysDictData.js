/**
 * 分页查询字典数据列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/sysDictData/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询字典数据详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/sysDictData/query/' + id,
        method: 'get'
    })
}

/**
 * 新增字典数据
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/sysDictData/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改字典数据
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/sysDictData/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除字典数据
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/sysDictData/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出字典数据
 *
 * @param query
 * @returns {*}
 */
function exportDictData(query) {
    return requests({
        url: '/admin/sysDictData/export',
        method: 'get',
        params: query
    })
}
