/**
 * 分页查询系统配置列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/sysConfig/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询系统配置详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/sysConfig/query/' + id,
        method: 'get'
    })
}

/**
 * 新增系统配置
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/sysConfig/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改系统配置
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/sysConfig/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除系统配置
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/sysConfig/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出系统配置
 *
 * @param query
 * @returns {*}
 */
function exportConfig(query) {
    return requests({
        url: '/admin/sysConfig/export',
        method: 'get',
        params: query
    })
}
